from django.http import request
from .models import User
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import UpdateView
from django.contrib import messages
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormMixin
from django.urls import reverse
from django.contrib import messages
# Create your views here.

def index_view(request):
    return render(request, 'user/index.html')

def sign_up_view(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = CustomUserCreationForm()
        
    return render(request, 'user/sign_up.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            #log le user dans django
            login(request, user)
            messages.success(request, "You're logged in !")
            return redirect('user-detail', pk=user.id)
    else:
        form = AuthenticationForm()
        
    return render(request, 'user/login.html', {'form': form})

@login_required(login_url='/login/')
def logout_view(request):
    logout(request)
    return redirect('home')

class UserDetailUpdateView(UpdateView, FormMixin):
    model = User
    form_class = CustomUserChangeForm
    template_name = "user/user_detail.html"

    # pass form_class to the template
    def get_context_data(self, **kwargs):
        context = super(UserDetailUpdateView, self).get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context

    # redirect to success url when form submitted (without ajax)
    def get_success_url(self):
        return reverse('user-detail', kwargs={'pk': self.object.id})

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


