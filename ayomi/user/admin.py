from django.contrib import admin
from django.contrib import admin
from user.models import User
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserChangeForm, CustomUserCreationForm

class MyUserAdmin(UserAdmin):
    list_display = ("email", "username", "last_name", "first_name","date_joined", "last_login", "phone_number","is_staff", )
    search_fields = ("email", "username", "is_staff")
    readonly_fields = ("date_joined", "last_login")

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(User, MyUserAdmin)
