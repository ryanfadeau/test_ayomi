from os import name
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_view, name='home'),
    path('login/', views.login_view, name="login"),
    path('logout/', views.logout_view, name="logout"),
    path('sign_up/', views.sign_up_view, name="sign_up"),
    path('user/<int:pk>/', views.UserDetailUpdateView.as_view(), name="user-detail"),
]
