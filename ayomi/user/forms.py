from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.db.models import fields
from .models import User

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('email', 'username', 'first_name', 'last_name', 'phone_number')

class CustomUserChangeForm(UserChangeForm):
    password = None
    class Meta:
        model = User
        fields = ('email',)
