const userID = document.querySelector('#userId').value;
const url = `http://127.0.0.1:8000/user/${userID}/`;
const csrftoken = Cookies.get('csrftoken');

let btn_submit = document.querySelector('#btn-submit');
let form = document.querySelector('#form');

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

btn_submit.addEventListener('click', e => {
    e.preventDefault();
    let user_new_email = document.querySelector('#id_email').value;
    valid_email = validateEmail(user_new_email);
    
    if(valid_email){
        const formData = new FormData(form);

        const request = new Request(url,
            {
                headers: {
                    'X-CSRFToken': csrftoken
                }
            }
        )
        fetch(request, {
            method: 'POST',
            mode: 'same-origin',
            body: formData
        }).then(response => {
            return response.text();
        }).then(response => {
            document.querySelector('#user-mail').innerHTML = user_new_email;
        }).catch(err => console.log(err));
    }

});